package ignitetest_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	keepertest "ignite-test/testutil/keeper"
	"ignite-test/testutil/nullify"
	"ignite-test/x/ignitetest"
	"ignite-test/x/ignitetest/types"
)

func TestGenesis(t *testing.T) {
	genesisState := types.GenesisState{
		Params: types.DefaultParams(),

		// this line is used by starport scaffolding # genesis/test/state
	}

	k, ctx := keepertest.IgnitetestKeeper(t)
	ignitetest.InitGenesis(ctx, *k, genesisState)
	got := ignitetest.ExportGenesis(ctx, *k)
	require.NotNil(t, got)

	nullify.Fill(&genesisState)
	nullify.Fill(got)

	// this line is used by starport scaffolding # genesis/test/assert
}
