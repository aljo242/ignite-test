package keeper_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	testkeeper "ignite-test/testutil/keeper"
	"ignite-test/x/ignitetest/types"
)

func TestGetParams(t *testing.T) {
	k, ctx := testkeeper.IgnitetestKeeper(t)
	params := types.DefaultParams()

	k.SetParams(ctx, params)

	require.EqualValues(t, params, k.GetParams(ctx))
}
